from pathlib import Path
from setuptools import setup

here = Path(__file__).parent

# Get the long description from the README file
with (here / 'README.rst').open(encoding='utf-8') as f:
    long_description = f.read()


setup(name='structured_config',
      packages=['structured_config'],
      description='Configuration module for python where the config structure is '\
                  'solely defined in python with full autocomplete support.',
      long_description=long_description,
      license='MIT',
      author='Andrew Leech',
      author_email='andrew@alelec.net',
      url='https://gitlab.com/alelec/structured_config',
      use_scm_version=True,
      install_requires=['setuptools', 'setuptools_scm', 'PyYAML', 'wrapt',
                        'appdirs', 'aenum', 'cryptography'],
      setup_requires=['setuptools_scm', 'pytest-runner'],
      tests_require=['pytest'],
      include_package_data=True
      )
