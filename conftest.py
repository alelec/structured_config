import pytest

CONF_YAML = 'yaml'
CONF_JSON = 'json'
CONF_PYTHON = 'py'


def pytest_addoption(parser):
    parser.addoption("--format", default=CONF_YAML, help="Run test for just one format")


@pytest.fixture(scope="module")
def fmt(request):
    return request.config.getoption('format')
