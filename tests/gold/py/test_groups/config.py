class config(Structure):
    class server(Structure):
        url = 'https://www.example.com'
        username = '<user>'
        password = '<password>'
    concurrent_connections = 64
    elem_a = 1
    elem_b = 3