import container, elem

class config(Structure):
    group = List(
        container(
            elems = List(
                elem(
                    a = 1,
                    b = 4,
                ),
            type=elem),
            ints = List(
                1,
                2,
            type=int),
            strs = List(
                '1',
                '2',
            type=str),
        ),
        container(
            elems = List(
                elem(
                    a = 1,
                    b = 2,
                ),
                elem(
                    a = 3,
                    b = 4,
                ),
            type=elem),
            ints = List(
                1,
                2,
            type=int),
            strs = List(
                '1',
                '2',
            type=str),
        ),
    type=container)