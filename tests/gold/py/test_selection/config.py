class Config(Structure):
    colour1 = 'red'
    colour2 = 'blue'
    colours = List(
        'red',
        'green',
        'blue',
    type=str)
    yesno = 'yes'
    new_sel = 'new_red'