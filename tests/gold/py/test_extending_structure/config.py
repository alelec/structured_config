class config(Structure):
    class server(Structure):
        url = 'https://www.example.com'
        username = '<user>'
        password = '<password>'
    concurrent_connections = 64
    new_field = 'write'