import os
import sys
import yaml
import json
import shutil
import pytest
import appdirs
import tempfile
import importlib
import subprocess
from pathlib import Path
from structured_config import ConfigFile, Structure, List
from structured_config.fields import Field, TypedField, IntField, BoolField, Selection, \
    SelectionField, MultiSelection, Deprecated, EncryptedField, StrField, Group
try:
    from conftest import *
except ImportError:
    # This satisfies pycharm
    from .conftest import *


def copy_result_to_new_gold(tf_file, gold_file):
    test_new_file = Path(gold_file).with_suffix(".test" + Path(gold_file).suffix)
    if test_new_file.exists():
        test_new_file.unlink()
    os.makedirs(gold_file.parent, exist_ok=True)
    shutil.copy(tf_file, test_new_file)

    try:
        if sys.platform.startswith('darwin'):
            subprocess.call(('open', str(test_new_file)))
        elif os.name == 'nt':  # For Windows
            os.startfile(str(test_new_file))
        elif os.name == 'posix':  # For Linux, Mac, etc.
            subprocess.call(('xdg-open', str(test_new_file)))
    except:
        pass


@pytest.fixture(scope="module")
def temp_dir():
    with tempfile.TemporaryDirectory() as tdir:
        tdir = Path(tdir)
        yield tdir

        for root, dirs, files in os.walk(tdir):
            for f in files:
                test_name = Path(root).name
                tf_file = Path(root) / f
                test_result = tf_file.read_text()
                gold_file = Path(__file__).parent / 'gold' / os.path.relpath(tf_file, tdir)
                try:
                    assert gold_file.exists(), f"No gold standard for {os.path.relpath(tf_file, tdir)}"
                    gold_result = gold_file.read_text()
                    assert test_result == gold_result, f"{test_name}: {tf_file} != {gold_file}"
                except AssertionError:
                    copy_result_to_new_gold(tf_file, gold_file)
                    raise


@pytest.fixture
def working_dir(temp_dir, fmt, request):
    test_name = request.node.name
    return temp_dir / fmt / test_name


@pytest.fixture
def conffile(working_dir, fmt):
    return working_dir / f"config.{fmt}"


def test_structure(conffile):
    importlib.reload(yaml)

    class config(Structure):
        class server(Structure):
            url = 'https://www.example.com'
            username = '<user>'
            password = '<password>'

        concurrent_connections = 32

    assert not os.path.exists(conffile), 'Config file should not already exist'

    Config = config(conffile)

    assert os.path.exists(conffile), 'Config file should exist'

    assert Config.server.url == 'https://www.example.com'
    assert Config.concurrent_connections == 32

    with pytest.raises(AttributeError):
        _ = Config.missing

def test_nofile_fields_structure():
    importlib.reload(yaml)

    class config(Structure):
        class server(Structure):
            url = StrField('https://www.example.com')  | "url of server"
            username = StrField('<user>')            | "login username"
            password = StrField('<password>')        | "login password"

        concurrent_connections = IntField(32)

    Config = config()

    assert Config.concurrent_connections == 32
    assert Config.server.url == 'https://www.example.com'

    with pytest.raises(AttributeError):
        _ = Config.missing


def test_write(conffile, fmt):
    importlib.reload(yaml)

    class config(Structure):
        class server(Structure):
            url = 'https://www.example.com'
            username = '<user>'
            password = '<password>'

        concurrent_connections = 32

    assert not os.path.exists(conffile), 'Config file should not already exist'

    Config = config(conffile)

    assert os.path.exists(conffile), 'Config file should exist'

    with open(conffile, 'r') as conffile_handle:
        orig_conf = conffile_handle.read()

    Config.concurrent_connections = 64

    with open(conffile, 'r') as conffile_handle:
        post_conf = conffile_handle.read()

    assert orig_conf != post_conf

    if fmt == CONF_PYTHON:
        assert 'class config' in post_conf
        assert 'class server' in post_conf
    elif fmt == CONF_JSON:
        assert '"config":' not in post_conf
        assert '"server":' in post_conf
    else:
        assert '!config' in post_conf
        assert '!server' in post_conf
    assert 'url' in post_conf
    assert 'https://www.example.com' in post_conf
    assert 'username' in post_conf
    assert 'password' in post_conf
    assert 'concurrent_connections' in post_conf
    assert '64' in post_conf


def test_round_trip(conffile):
    importlib.reload(yaml)

    class config(Structure):
        class server(Structure):
            url = 'https://www.example.com'
            username = '<user>'
            password = '<password>'

        concurrent_connections = 32

    assert not os.path.exists(conffile), 'Config file should not already exist'

    Config = config(conffile)

    Config.concurrent_connections = 64

    config_file2 = ConfigFile(conffile, config)

    assert config_file2.config.concurrent_connections == 64

    assert config_file2.config.server == Config.server

    assert config_file2.config.server.url == Config.server.url
    assert config_file2.config.server.username == Config.server.username
    assert config_file2.config.server.password == Config.server.password


def test_extending_structure(conffile):
    importlib.reload(yaml)

    class config(Structure):
        class server(Structure):
            url = 'https://www.example.com'
            username = '<user>'
            password = '<password>'

        concurrent_connections = 32

    assert not os.path.exists(conffile), 'Config file should not already exist'

    Config = config(conffile)

    Config.concurrent_connections = 64

    class config(Structure):
        class server(Structure):
            url = 'https://www.example.com'
            username = '<user>'
            password = '<password>'

        concurrent_connections = 32
        new_field = "test"

    config_file2 = ConfigFile(conffile, config)

    assert config_file2.config.concurrent_connections == 64

    assert config_file2.config.server == Config.server

    assert config_file2.config.server.url == Config.server.url
    assert config_file2.config.server.username == Config.server.username
    assert config_file2.config.server.password == Config.server.password
    config_file2.config.new_field = "write"

    config_file3 = ConfigFile(conffile, config)

    assert config_file3.config.new_field == "write"


def test_attrib_structure(conffile):
    importlib.reload(yaml)

    class elem(Structure):
        a = None
        b = None

    class config(Structure):
        first = elem()
        second = elem(a=3, b=4)
        third = elem

    assert not os.path.exists(conffile), 'Config file should not already exist'

    Config = config(conffile)

    assert Config.first.a == Config.third.a
    assert Config.second.b == 4

    config_file2 = ConfigFile(conffile, config)
    Config2 = config_file2.config  # type: config

    assert Config2.first.a == Config2.third.a
    assert Config2.second.b == 4


def test_nested_write(conffile):
    importlib.reload(yaml)

    class config(Structure):
        class server(Structure):
            url = 'https://www.example.com'
            username = '<user>'
            password = '<password>'

        concurrent_connections = 32

    Config = config(conffile)

    new_url = 'http://www.new.url'
    Config.server.url = new_url

    config_file2 = ConfigFile(conffile, config)

    assert config_file2.config.server.url == new_url


def test_list(conffile):
    importlib.reload(yaml)

    class elem(Structure):
        a = None
        b = None

    class config(Structure):
        group = List(
            elem(a=10, b=11),
            elem(a=12, b=13),
            elem(a=14, b=15),
        type=elem)

    Config = config(conffile)

    # Check the initial settings are still as expected
    assert len(Config.group) == 3
    assert Config.group[0].a == 10 and Config.group[0].b == 11

    # Check that the conf file reloaded matches
    config_file2 = ConfigFile(conffile, config)
    assert config_file2.config.group == Config.group
    Config = config_file2.config  # type: config

    # Change a value on one of the elem and check reloaded matches
    Config.group[0].a = 1

    config_file3 = ConfigFile(conffile, config)
    assert config_file3.config.group[0].a == 1
    Config = config_file3.config  # type: config

    # Change elems in the list and check reloaded matches
    Config.group.pop(0)

    config_file4 = ConfigFile(conffile, config)
    assert len(config_file4.config.group) == 2
    assert config_file4.config.group[0].a == 12
    assert config_file4.config.group[1].a == 14

    assert set(dir(config_file4.config)) == {'__as_dict__', '__update__', 'group'}
    assert isinstance(repr(config_file4.config), str)

    assert dir(config_file4.config.group)
    assert isinstance(repr(config_file4.config.group), str)

    assert dir(config_file4.config.group[0])
    assert isinstance(repr(config_file4.config.group[0]), str)


def test_typed_fields(conffile, fmt):
    importlib.reload(yaml)

    class config(Structure):
        concurrent_connections = IntField(32)
        path = TypedField('PATH', os.environ.get, store_converted=False)

    assert not os.path.exists(conffile), 'Config file should not already exist'

    Config = config(conffile)

    assert Config.path != 'PATH'

    if fmt == CONF_YAML:
        assert ': PATH\n' in Path(conffile).read_text()
    elif fmt == CONF_PYTHON:
        assert "'PATH'" in Path(conffile).read_text()
    elif fmt == CONF_JSON:
        assert '"PATH"' in Path(conffile).read_text()

    Config.concurrent_connections = '64'

    with pytest.raises(ValueError):
        Config.concurrent_connections = 'abc'

    assert Config.concurrent_connections == 64
    assert len(Config.path)
    assert Config.path != 'PATH'

    config_file2 = ConfigFile(conffile, config)

    assert config_file2.config.concurrent_connections == 64
    assert len(config_file2.config.path)
    assert config_file2.config.path == Config.path

    assert dir(config_file2.config)
    assert isinstance(repr(config_file2.config.concurrent_connections), str)


def test___to_dict__(conffile):
    importlib.reload(yaml)

    class item(Structure):
        a = 30

    class config(Structure):
        class server(Structure):
            url = 'https://www.example.com'
            username = '<user>'
            password = '<password>'

        concurrent_connections = 32

        l = [
            10,
            20,
            item()
        ]

    assert not os.path.exists(conffile), 'Config file should not already exist'

    Config = config(conffile)

    assert isinstance(Config['l'], List)
    assert isinstance(Config.l, List)
    assert Config.l[2].a == 30

    d = Config.__as_dict__()
    assert isinstance(d, dict), 'top level config should be a dict'
    assert isinstance(d['server'], dict)
    assert not isinstance(d['l'], List), 'List should become list'
    assert d['server']['username'] == '<user>'
    assert d['l'][1] == 20


def test_relative_config_no_appdir(conffile):
    importlib.reload(yaml)

    class config(Structure):
        class server(Structure):
            url = 'https://www.example.com'
            username = '<user>'
            password = '<password>'

        concurrent_connections = 32

    conffile = Path(conffile).name

    with pytest.raises(ValueError):
        config(conffile)


def test_relative_config_with_appdir(monkeypatch, working_dir, conffile):
    importlib.reload(yaml)

    class config(Structure):
        class server(Structure):
            url = 'https://www.example.com'
            username = '<user>'
            password = '<password>'

        concurrent_connections = 32

    monkeypatch.setattr(appdirs, "user_data_dir", lambda appname: str(working_dir))

    confname = Path(conffile).name

    Config = config(confname, "test")

    assert os.path.exists(conffile)


def test_Dict(conffile):
    importlib.reload(yaml)

    class config(Structure):
        a_key = 32
        random_data = {
            'one': 1
        }

    Config = config(conffile)
    Config.random_data['two'] = 2

    Config2 = config(conffile)

    assert Config2.random_data['one'] == 1
    assert Config2.random_data['two'] == 2


def test_selection(conffile):
    importlib.reload(yaml)

    class NewColours(Selection):
        new_ultraviolet = ''
        new_red = ''
        new_green = ''

    class Colours(Selection):
        ultraviolet = ''
        red = ''
        green = ''
        blue = ''
        magenta = ''
        cyan = ''
        white = ''

    class Config(Structure):
        colour1 = SelectionField('red', Colours)
        colour2 = SelectionField(Colours.blue, Colours)
        colours = MultiSelection([], Colours)
        yesno = SelectionField('yes', ['yes', 'no'])

        new_sel = SelectionField(NewColours.new_red, NewColours)

        dep_sel = Deprecated(SelectionField('red', Colours),
                             new_fieldname='new_sel',
                             converter=lambda x: getattr(Colours, str(x)))

        dep_mul = Deprecated(MultiSelection([Colours.ultraviolet], Colours),
                             new_fieldname='new_sel',
                             converter=lambda x: getattr(Colours, str(x[0])) if x else tuple())

    config = Config(conffile)

    assert config.colour1 == Colours.red
    assert config.colour1 == 'red'

    assert config.colour1 != 'blue'

    assert config.colour2 == Colours.blue
    assert config.colour2 == 'blue'

    with pytest.raises(KeyError):
        config.colour2 = "none"

    assert config.yesno == 'yes'

    with pytest.raises(KeyError):
        config.yesno = "none"

    config.colours = [Colours.red, Colours.green, 'blue']
    assert len(config.colours) == 3
    assert 'green' in config.colours
    assert Colours.blue in config.colours
    with pytest.raises(KeyError):
        config.colours = [Colours.red, 'dead']

    with pytest.raises((KeyError, AttributeError)):
        config.colours.append('dead')

    # Reload from file
    config = Config(conffile)

    config.colours = [Colours.red, Colours.green, 'blue']
    assert len(config.colours) == 3
    assert 'green' in config.colours
    assert Colours.blue in config.colours


def test_doc(conffile):
    importlib.reload(yaml)

    class Config(Structure):
        zero = Field(0)                              | "zero"
        nope = BoolField('no')                       | "should be false"
        three = SelectionField('3', ['1', '2', '3']) | "three"

    config = Config(conffile)

    assert config.zero == 0
    assert 'zero' in config.__fdoc__('zero')

    assert 'false' in config.__fdoc__('nope')
    assert config.nope is False

    assert 'three' in config.__fdoc__('three')
    assert config.three == '3'
    config.three = '1'
    del config

    config2 = Config(conffile)

    assert config2.zero == 0
    assert 'zero' in config2.__fdoc__('zero')

    assert 'false' in config2.__fdoc__('nope')
    assert config2.nope is False

    assert 'three' in config2.__fdoc__('three')
    assert config2.three == '1'


def test_Deprecated(working_dir, conffile):
    importlib.reload(yaml)

    class Config(Structure):
        old_key = IntField(1)

    # Write old key to conf
    conffile1 = working_dir / '1' / conffile.name
    Config(conffile1)

    # Update definition of Config
    importlib.reload(yaml)

    class Config(Structure):
        new_key = BoolField('no')
        old_key = Deprecated(IntField(1), 'new_key')

    config = Config(conffile1)

    assert config.new_key, "New key should be set by old key in conf"

    # TODO CHECK IF ACCESS PRESENTS WARNING
    # with pytest.raises(AttributeError):
    #     _ = config.old_key   # Should not be trying to access old key in software

    # update config to force a write
    config.new_key = False
    assert 'old_key' not in conffile1.read_text(), "Deprecated field should not be written"

    config2 = Config(conffile1)

    assert not config2.new_key, "new key should have been written false above"

    # Start new config file
    importlib.reload(yaml)

    class Config(Structure):
        new_key = BoolField('no')
        old_key = Deprecated(IntField(1), 'new_key')

    conffile2 = working_dir / '2' / conffile1.name
    config = Config(conffile2)

    assert not config.new_key, "Old key should not overwrite new value from definition"

    # TODO CHECK IF ACCESS PRESENTS WARNING
    # with pytest.raises(AttributeError):
    #     _ = config.old_key   # Should not be trying to access old key in software
    importlib.reload(yaml)

    class Config(Structure):
        new_key = BoolField('no')
        old_key = Deprecated(IntField(1), 'new_key')


def test_Deprecated_sel(working_dir, conffile):
    class NewColours(Selection):
        new_ultraviolet = ''
        new_red = ''
        new_green = ''

    class Colours(Selection):
        ultraviolet = ''
        red = ''
        green = ''
        blue = ''
        magenta = ''
        cyan = ''
        white = ''

    class Config(Structure):
        dep_sel = SelectionField('red', Colours)

    _ = Config(conffile)

    def converter(colour):
        return NewColours[f"new_{colour}"]

    class Config(Structure):
        new_sel = SelectionField(NewColours.new_red, NewColours)

        dep_sel = Deprecated(SelectionField('red', Colours),
                             new_fieldname='new_sel',
                             converter=converter)

    conf = Config(conffile)
    conf.new_sel = NewColours.new_green


def test_Encrypted(monkeypatch, conffile):
    importlib.reload(yaml)

    # The real encrypion library gives a different result each time, so
    # we mock it here to create an un-protected but consistent crypt
    def mock_encrypt(self, data):
        bdata = data.encode() if isinstance(data, str) else data
        if data is None or bdata.startswith(self.crypt_header):
            return data
        return self.crypt_header.decode() + json.dumps({"pass": self.PASSWORD, "val": "".join(reversed(data))})

    def mock_decrypt(self, data):
        bdata = data.encode() if isinstance(data, str) else data
        if data is None or not bdata.startswith(self.crypt_header):
            return data
        loaded = json.loads(data[len(self.crypt_header):])
        assert loaded['pass'] == self.PASSWORD
        return "".join(reversed(loaded['val']))

    monkeypatch.setattr(EncryptedField, "encrypt", mock_encrypt)
    monkeypatch.setattr(EncryptedField, "decrypt", mock_decrypt)

    EncryptedField.PASSWORD = "test"

    class config(Structure):
        secret = EncryptedField("unsafe")
        empty = EncryptedField(None)

    Config = config(conffile)

    assert Config.secret == "unsafe"

    assert "unsafe" not in conffile.read_text()
    assert "ENC:" in conffile.read_text()

    Config.secret = "open"

    assert "open" not in conffile.read_text()
    assert "ENC:" in conffile.read_text()

    Config2 = config(conffile)

    assert Config2.secret == "open"
    assert Config2.empty is None


def test_list_in_list(conffile):
    importlib.reload(yaml)

    class elem(Structure):
        a = 1
        b = 2

    class container(Structure):
        elems = List(
            elem,
            elem(a=3, b=4),
        type=elem)
        ints = List(
            1,
            2,
        type=int)
        strs = List(
            "1",
            "2",
        type=str)

    class config(Structure):
        group = List(
            container,
            container(),
        type=container)

    Config = config(conffile)

    # Check the initial settings are still as expected
    assert len(Config.group) == 2
    assert len(Config.group[0].elems) == 2
    assert Config.group[0].elems[1].a == 3
    assert Config.group[0].elems[0].b == 2

    assert Config.group[0].elems[1]._config_file is not None
    assert Config.group[0].elems[0]._config_file is not None

    # Check that the conf file reloaded matches
    config_file2 = ConfigFile(conffile, config)
    assert config_file2.config.group == Config.group
    Config = config_file2.config  # type: config

    assert Config.group[0].elems[1]._config_file is not None
    assert Config.group[0].elems[0]._config_file is not None

    # Change a value on one of the elem and check reloaded matches
    Config.group[0].elems[1].a = 1

    config_file3 = ConfigFile(conffile, config)
    assert config_file3.config.group[0].elems[1].a == 1
    Config = config_file3.config  # type: config

    # Change elems in the list and check reloaded matches
    Config.group[0].elems.pop(0)

    config_file4 = ConfigFile(conffile, config)
    assert len(config_file4.config.group) == 2
    assert config_file4.config.group[0].elems[0].a == 1


def test_typed_list(conffile):
    importlib.reload(yaml)

    class elem(Structure):
        a = 1
        b = 2

    class config(Structure):
        elems = List(
            elem,
            elem(a=3, b=4),
        type=elem)

    Config = config(conffile)

    # Check the initial settings are still as expected
    assert len(Config.elems) == 2
    assert Config.elems[1].a == 3
    assert isinstance(Config.elems[1], elem)

    with pytest.raises(ValueError):
        Config.elems.insert(0, "Not elem")

    with pytest.raises(ValueError):
        Config.elems.append("Not elem")

    Config.elems.append(elem(a=4, b=50))
    assert len(Config.elems) == 3


def test_load_string(conffile, fmt):
    importlib.reload(yaml)

    class config(Structure):
        class server(Structure):
            url = 'https://www.example.com'
            username = '<user>'
            password = '<password>'

        concurrent_connections = 32

    assert not os.path.exists(conffile), 'Config file should not already exist'

    Config = config(conffile)

    Config.concurrent_connections = 64

    if fmt == CONF_YAML:
        yml = Path(conffile).read_text()
        assert yml == Config.__getyaml__()
    else:
        yml = Config.__getyaml__()

    config2 = config()
    config2.__setyaml__(yml)

    assert config2.concurrent_connections == 64

    assert config2.server == Config.server

    assert config2.server.url == Config.server.url
    assert config2.server.username == Config.server.username
    assert config2.server.password == Config.server.password


def test_subclassed_structure(conffile, fmt):
    importlib.reload(yaml)

    class parent_config(Structure):
        concurrent_connections = IntField(32)

    class config(parent_config):
        path = TypedField('PATH', os.environ.get, store_converted=False)

    assert not os.path.exists(conffile), 'Config file should not already exist'

    Config = config(conffile)

    assert Config.path != 'PATH'
    assert Config.concurrent_connections == 32

    if fmt == CONF_YAML:
        assert ': PATH\n' in Path(conffile).read_text()
    elif fmt == CONF_PYTHON:
        assert "'PATH'" in Path(conffile).read_text()
    elif fmt == CONF_JSON:
        assert '"PATH"' in Path(conffile).read_text()

    Config.concurrent_connections = '64'

    with pytest.raises(ValueError):
        Config.concurrent_connections = 'abc'

    assert parent_config.concurrent_connections.value == 32, "class definition should be changed"
    assert Config.concurrent_connections == 64
    assert len(Config.path)
    assert Config.path != 'PATH'

    config_file2 = ConfigFile(conffile, config)

    assert config_file2.config.concurrent_connections == 64
    assert len(config_file2.config.path)
    assert config_file2.config.path == Config.path

    assert dir(config_file2.config)
    assert isinstance(repr(config_file2.config.concurrent_connections), str)


def test_subclass_promotion(conffile, fmt):
    importlib.reload(yaml)

    class parent_config(Structure):
        concurrent_connections = IntField(32)

    class config(parent_config):
        path = TypedField('PATH', os.environ.get, store_converted=False)

    assert not os.path.exists(conffile), 'Config file should not already exist'

    _parent_config = parent_config(conffile)

    assert os.path.exists(conffile), 'Config file should already'

    if fmt == CONF_YAML:
        assert ': PATH\n' not in Path(conffile).read_text()
    elif fmt == CONF_PYTHON:
        assert "'PATH'" not in Path(conffile).read_text()
    elif fmt == CONF_JSON:
        assert '"PATH"' not in Path(conffile).read_text()

    _subclass = config(conffile)

    # Shouldn't have written anything yet
    if fmt == CONF_YAML:
        assert ': PATH\n' not in Path(conffile).read_text()
    elif fmt == CONF_PYTHON:
        assert "'PATH'" not in Path(conffile).read_text()
    elif fmt == CONF_JSON:
        assert '"PATH"' not in Path(conffile).read_text()

    _subclass.concurrent_connections = 10

    # Now should have written
    if fmt == CONF_YAML:
        assert ': PATH\n' in Path(conffile).read_text()
    elif fmt == CONF_PYTHON:
        assert "'PATH'" in Path(conffile).read_text()
    elif fmt == CONF_JSON:
        assert '"PATH"' in Path(conffile).read_text()


def test_groups(conffile):
    importlib.reload(yaml)

    class config(Structure):
        server_details = Group()

        class server(Structure):
            url = 'https://www.example.com'
            username = '<user>'
            password = '<password>'

        concurrent_connections = 32

        __elems__ = Group()
        elem_a = 1
        elem_b = 2

    assert not os.path.exists(conffile), 'Config file should not already exist'

    Config = config(conffile)

    Config.concurrent_connections = 64

    Config.__elems__.elem_b = 3
    assert Config.elem_b == 3

    config_file2 = ConfigFile(conffile, config)
    assert config_file2.config.concurrent_connections == 64
    assert config_file2.config.server == Config.server
    assert config_file2.config.server.url == Config.server.url
    assert config_file2.config.server.username == Config.server.username
    assert config_file2.config.server.password == Config.server.password

    assert len(Config.server_details) == 2
    assert len(Config.__elems__) == 2

    assert config_file2.config.__elems__.elem_b == 3
    assert config_file2.config.elem_b == 3

